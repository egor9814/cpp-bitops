//
// Created by egor9814 on 03 Aug 2021.
//

#define TestVisibleEnabled
#include <stest.hpp>
#include <bitops/vector.hpp>

AutoTestUnit(BitVectorTests)
	using namespace bitops;

	AutoTestCase(ZeroLenIsNull) {
		BitVector bv;
		TestVisibleResolve(BitVector) r{bv};
		TestCheck(r->size() == 0);
		TestCheck(r->len() == 0);
		TestCheck(r->data() == nullptr);
	}

	AutoTestCase(FourBitsIsOneByte) {
		BitVector bv{4};
		TestCheck(bv.getSize() == 1);
		TestCheck(bv.getLength() == 4);
	}

	AutoTestCase(FourteenBitsIsTwoBytes) {
		BitVector bv{14};
		TestCheck(bv.getSize() == 2);
		TestCheck(bv.getLength() == 14);
	}

	AutoTestCase(SixteenBitsIsTwoBytes) {
		BitVector bv{16};
		TestCheck(bv.getSize() == 2);
		TestCheck(bv.getLength() == 16);
	}

	AutoTestCase(Default) {
		BitVector bv(14);
		bv.set1(2);
		bv.set1(4);
		bv[11].set();

		TestCheck(bv.toString() == "00100000010100");
	}

	AutoTestCase(ParseString) {
		std::string src{"100111010100110010101111010001010111010100111010101"};
		BitVector bv(src.data());
		TestCheck(bv.toString() == src);
	}

	AutoTestCase(Invert) {
		std::string src{"00100000010100"};
		std::string dst{"00100010010001"};
		BitVector bv(src.data());
		bv.invert(0);
		bv.invert(2);
		bv.invert(7);

		TestCheck(bv.toString() == dst);
	}

	AutoTestCase(Equality) {
		BitVector bv1("00100000010100");
		BitVector bv2("00101000000100");
		BitVector bv3("00100000010100");

		TestCheck(bv1 != bv2);
		TestCheck(bv1 == bv3);
		TestCheck(bv2 != bv3);
	}

	AutoTestCase(Conjunction) {
		BitVector bv1("00100000010100");
		BitVector bv2("00101000000100");
		BitVector bv3("00100000000100");

		TestCheck((bv1 & bv2) == bv3);
	}

	AutoTestCase(Disjunction) {
		BitVector bv1("00100000010100");
		BitVector bv2("00101000000100");
		BitVector bv3("00101000010100");

		TestCheck((bv1 | bv2) == bv3);
	}

	AutoTestCase(Xor) {
		BitVector bv1("00100000010100");
		BitVector bv2("00101000000100");
		BitVector bv3("00001000010000");

		TestCheck((bv1 ^ bv2) == bv3);
	}

	AutoTestCase(Iversion) {
		BitVector bv1("00100000010100");
		BitVector bv2("11011111101011");

		TestCheck(~bv1 == bv2);
	}

	AutoTestCase(ShiftLeft) {
		BitVector bv1("00100000010100"); // 0
		BitVector bv2("10000001010000"); // 2
		BitVector bv3("01010000000000"); // 8
		BitVector bv4(bv3.getLength()); // 12 or zero vector

		TestCheck((bv1 << 0) == bv1);
		TestCheck((bv1 << 2) == bv2);
		TestCheck((bv1 << 8) == bv3);
		TestCheck((bv1 << 12) == bv4);
	}

	AutoTestCase(ShiftRight) {
		BitVector bv1("00100000010100"); // 0
		BitVector bv2("00001000000101"); // 2
		BitVector bv3("00000000001000"); // 8
		BitVector bv4(bv3.getLength()); // 12 or zero vector

		TestCheck((bv1 >> 0) == bv1);
		TestCheck((bv1 >> 2) == bv2);
		TestCheck((bv1 >> 8) == bv3);
		TestCheck((bv1 >> 12) == bv4);
	}

EndTestUnit(BitVectorTests)
