//
// Created by egor9814 on 04 Aug 2021.
//

#define TestVisibleEnabled
#include <stest.hpp>
#include <bitops/interval.hpp>

AutoTestUnit(TriBitRefTests)

	AutoTestCase() {
		using namespace bitops;

		BitVector vec{6};
		BitVector dnc{6};

		BitInterval::TriBitRef ref{&vec, &dnc, 1};
		TestVisibleResolve(BitInterval::TriBitRef) r{ref};

		TestCheck(vec.toString() == "000000" && dnc.toString() == "000000");

		ref.set();
		TestCheck(vec.toString() == "000010" && dnc.toString() == "000000");

		r->index()++;
		ref.set();
		TestCheck(vec.toString() == "000110" && dnc.toString() == "000000");

		ref.x();
		TestCheck(vec.toString() == "000010" && dnc.toString() == "000100");
	}

EndTestUnit(TriBitRefTests)
