//
// Created by egor9814 on 03 Aug 2021.
//

#define TestVisibleEnabled
#include <stest.hpp>
#include <bitops/vector.hpp>

AutoTestUnit(BitRefTests)

AutoTestCase() {
	bitops::byte_t byte{0b00001010};
	bitops::BitVector::BitRef ref(&byte, bitops::byte_t(1));
	TestVisibleResolve(bitops::BitVector::BitRef) r{ref};
	TestCheck(ref.get() == true);
	r->shift()++;
	TestCheck(ref.get() == false);
	r->shift()++;
	TestCheck(ref.get() == true);
}

EndTestUnit(BitRefTests)
