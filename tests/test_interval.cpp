//
// Created by egor9814 on 04 Aug 2021.
//

#define TestVisibleEnabled
#include <stest.hpp>
#include <bitops/interval.hpp>

AutoTestUnit(BitIntervalTests)
	using namespace bitops;

	AutoTestCase(ZeroLenIsEmptyVectors) {
		BitInterval bi;
		TestVisibleResolve(BitInterval) r{bi};

		TestCheck(r->vec().getSize() == 0);
		TestCheck(r->dnc().getSize() == 0);
	}

	AutoTestCase(Default) {
		BitInterval bi{5};
		TestCheck(bi.toString() == "00000");
	}

	AutoTestCase(ParseStrings) {
		BitInterval bi1{"1001101", "1124"};
		TestVisibleResolve(BitInterval) r1{bi1};

		BitInterval bi2{"1001101", "1200340"};
		TestVisibleResolve(BitInterval) r2{bi2};

		TestCheck(r1->vec().getSize() == 0);
		TestCheck(r1->dnc().getSize() == 0);

		TestCheck(r2->vec().toString() == "1001101");
		TestCheck(r2->dnc().toString() == "1100110");
	}

	AutoTestCase(ParseString) {
		BitInterval bi{"102-1-x"};
		TestVisibleResolve(BitInterval) r{bi};

		TestCheck(r->vec().toString() == "1010101");
		TestCheck(r->dnc().toString() == "0001010");
	}

	AutoTestCase(FromVectors) {
		BitVector bv1{"0100100"};
		BitVector bv2{"0110"};
		BitVector bv3{"0011011"};

		BitInterval bi1{bv1, bv2};
		BitInterval bi2{bv1, bv3};
		BitInterval bi3{bv2, bv3};
		BitInterval bi4{bv2, ~bv2};

		TestCheck(bi1.toString().empty());
		TestCheck(bi2.toString() == "01--1--");
		TestCheck(bi3.toString().empty());
		TestCheck(bi4.toString() == "-11-");
	}

	AutoTestCase(Rang) {
		BitInterval bi0{"1001110"};
		BitInterval bi1{"10-1110"};
		BitInterval bi4{"-0-11--"};

		TestCheck(bi0.getRang() == 7);
		TestCheck(bi1.getRang() == 6);
		TestCheck(bi4.getRang() == 3);
	}

	AutoTestCase(Orthogonal) {
		BitInterval bi1{"1001110"};
		BitInterval bi2{"1001100"};
		BitInterval bi3{"1001010"};
		BitInterval bi4{"1001000"};
		BitInterval bi5{"100-110"};
		BitInterval bi6{"100-100"};
		BitInterval bi7{"100-010"};
		BitInterval bi8{"100-000"};

		TestCheck(!bi1.isOrthogonal(bi1));
		TestCheck(bi1.isOrthogonal(bi2));
		TestCheck(bi1.isOrthogonal(bi3));
		TestCheck(!bi1.isOrthogonal(bi4));
		TestCheck(!bi1.isOrthogonal(bi5));

		TestCheck(bi1.isOrthogonal(bi4, false));

		TestCheck(!bi5.isOrthogonal(bi5));
		TestCheck(bi5.isOrthogonal(bi6));
		TestCheck(bi5.isOrthogonal(bi7));
		TestCheck(!bi5.isOrthogonal(bi8));

		TestCheck(bi5.isOrthogonal(bi8, false));
	}

	AutoTestCase(Disjunction) {
		using namespace bitops::literals;
		TestCheck(("000-0-"_bi | "010-0-"_bi) == "010-0-"_bi);
		TestCheck(("00010-"_bi | "010-0-"_bi) == "01010-"_bi);
		TestCheck(("1-1-1-"_bi | "1--010"_bi) == "1-1010"_bi);
	}

	AutoTestCase(Conjunction) {
		using namespace bitops::literals;
		TestCheck(("010-1-"_bi & "1-1111"_bi) == "0-0-1-"_bi);
		TestCheck(("1-11--"_bi & "-11---"_bi) == "--1---"_bi);
	}

	AutoTestCase(Inversion) {
		using namespace bitops::literals;
		TestCheck(~ "1-1-1-"_bi == "0-0-0-"_bi);
		TestCheck(~ "1--010"_bi == "0--101"_bi);
	}

EndTestUnit(BitIntervalTests)
