//
// Created by egor9814 on 02 Aug 2021.
//

#ifndef BITOPS_VECTOR_HPP
#define BITOPS_VECTOR_HPP

#include <string>
#include <cstdint>

#ifdef BITOPS_TESTS_ENABLED
#include <stest.hpp>
#endif //BITOPS_TESTS_ENABLED

namespace bitops {

	using byte_t = unsigned char;
	using std::size_t;

	class BitVector {
		byte_t *data;
		size_t len, size;

	public:
		class BitRef {
			friend class BitVector;
			byte_t *byte;
			byte_t shift;

#ifdef BITOPS_TESTS_ENABLED
		// Enable public constructor for testing
		public:
#endif //BITOPS_TESTS_ENABLED
			BitRef(byte_t *byte, byte_t shift);

		public:
			BitRef(const BitRef &) = delete;

			BitRef &operator=(const BitRef &ref);

			BitRef(BitRef &&ref) noexcept;
			BitRef &operator=(BitRef &&ref) noexcept;

			BitRef &set(bool bit = true) noexcept;

			BitRef &reset() noexcept;

			[[nodiscard]] bool get() const noexcept;

			BitRef &operator=(bool bit) noexcept;

			explicit operator bool() const noexcept;

#ifdef BITOPS_TESTS_ENABLED
			// Enable public visibility for private fields
			TestVisible(BitRef)
				TestProperty(byte)
				TestProperty(shift)
			TestVisibleEnd()
#endif //BITOPS_TESTS_ENABLED
		};

		explicit BitVector(size_t len = 0);

		explicit BitVector(const char *str);

		BitVector(const BitVector &bv);

		BitVector(BitVector &&bv) noexcept;

		BitVector &operator=(const BitVector &bv);

		BitVector &operator=(BitVector &&bv) noexcept;

		~BitVector();

		void setString(const char *str) noexcept;

		BitVector &operator=(const char *str) noexcept;

		BitVector &set0(size_t k);

		BitVector &set1(size_t k);

		BitVector &invert(size_t k);

		bool operator==(const BitVector &other) const noexcept;

		bool operator!=(const BitVector &other) const noexcept;

		BitVector &operator&=(const BitVector &other) noexcept;

		BitVector operator&(const BitVector &other) const noexcept;

		BitVector &operator|=(const BitVector &other) noexcept;

		BitVector operator|(const BitVector &other) const noexcept;

		BitVector &operator^=(const BitVector &other) noexcept;

		BitVector operator^(const BitVector &other) const noexcept;

		BitVector operator~() const noexcept;

		BitVector &operator<<=(size_t k) noexcept;

		BitVector operator<<(size_t k) const noexcept;

		BitVector &operator>>=(size_t k) noexcept;

		BitVector operator>>(size_t k) const noexcept;

		BitRef operator[](size_t k);

		BitRef operator[](size_t k) const;

		[[nodiscard]] std::string toString() const noexcept;

		explicit operator std::string() const noexcept;

		[[nodiscard]] size_t getWeight() const noexcept;

		[[nodiscard]] size_t getSize() const noexcept;

		[[nodiscard]] size_t getLength() const noexcept;

#ifdef BITOPS_TESTS_ENABLED
		TestVisible(BitVector)
			TestProperty(data)
			TestProperty(len)
			TestProperty(size)
		TestVisibleEnd()
#endif //BITOPS_TESTS_ENABLED
	};

	namespace literals {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wuser-defined-literals"
		inline BitVector operator""_bv(const char *str, size_t) noexcept {
			return BitVector{str};
		}
#pragma clang diagnostic pop
	}

}

#endif //BITOPS_VECTOR_HPP
