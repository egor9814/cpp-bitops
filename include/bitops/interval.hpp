//
// Created by egor9814 on 03 Aug 2021.
//

#ifndef BITOPS_INTERVAL_HPP
#define BITOPS_INTERVAL_HPP

#include <bitops/vector.hpp>

#ifdef BITOPS_TESTS_ENABLED
#include <stest.hpp>
#endif //BITOPS_TESTS_ENABLED

namespace bitops {

	class BitInterval {
		BitVector vec, dnc;

	public:
		enum class TriBit {
			Zero = 0,
			One,
			X
		};

		class TriBitRef {
			friend class BitInterval;
			BitVector *vec, *dnc;
			size_t index;

#ifdef BITOPS_TESTS_ENABLED
			// Enable public constructor for testing
		public:
#endif //BITOPS_TESTS_ENABLED
			TriBitRef(BitVector *vec, BitVector *dnc, size_t index);

		public:
			TriBitRef(const TriBitRef &) = delete;

			TriBitRef &operator=(const TriBitRef &ref);

			TriBitRef(TriBitRef &&ref) noexcept;
			TriBitRef &operator=(TriBitRef &&ref) noexcept;

			TriBitRef &set(TriBit bit = TriBit::One);

			TriBitRef &reset();

			TriBitRef &x();

			[[nodiscard]] TriBit get() const;

			TriBitRef &operator=(TriBit bit);

			explicit operator TriBit() const;

#ifdef BITOPS_TESTS_ENABLED
			// Enable public visibility for private fields
			TestVisible(TriBitRef)
				TestProperty(vec)
				TestProperty(dnc)
				TestProperty(index)
			TestVisibleEnd()
#endif //BITOPS_TESTS_ENABLED
		};

		explicit BitInterval(size_t len = 0) noexcept;

		BitInterval(const char *vec, const char *dnc) noexcept;

		explicit BitInterval(const char *str) noexcept;

		BitInterval(const BitVector &vec, const BitVector &dnc) noexcept;

		BitInterval(const BitInterval &bi);

		BitInterval(BitInterval &&bi) noexcept;

		BitInterval &operator=(const BitInterval &bi);

		BitInterval &operator=(BitInterval &&bi) noexcept;

		void setInterval(const BitVector &vec, const BitVector &dnc) noexcept;

		void setString(const char *interval) noexcept;

		bool operator==(const BitInterval &other) const noexcept;

		bool operator!=(const BitInterval &other) const noexcept;

		[[nodiscard]] std::string toString() const noexcept;

		explicit operator std::string() const noexcept;

		[[nodiscard]] size_t getLength() const noexcept;

		[[nodiscard]] size_t getRang() const noexcept;

		[[nodiscard]] bool isOrthogonal(const BitInterval &other, bool one = true) const noexcept;

		[[nodiscard]] TriBit get(size_t k) const;

		void set(size_t k, TriBit bit);

		TriBitRef operator[](size_t k);

		TriBitRef operator[](size_t k) const;

		BitInterval &operator|=(const BitInterval &other) noexcept;

		BitInterval operator|(const BitInterval &other) const noexcept;

		BitInterval &operator&=(const BitInterval &other) noexcept;

		BitInterval operator&(const BitInterval &other) const noexcept;

		BitInterval operator~() const noexcept;

		BitInterval &operator<<=(size_t k) noexcept;

		BitInterval operator<<(size_t k) const noexcept;

		BitInterval &operator>>=(size_t k) noexcept;

		BitInterval operator>>(size_t k) const noexcept;

		[[nodiscard]] const BitVector &getVector() const noexcept;

		[[nodiscard]] const BitVector &getDnc() const noexcept;

#ifdef BITOPS_TESTS_ENABLED
		TestVisible(BitInterval)
			TestProperty(vec)
			TestProperty(dnc)
		TestVisibleEnd()
#endif //BITOPS_TESTS_ENABLED
	};

	namespace literals {
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Wuser-defined-literals"
		inline BitInterval operator""_bi(const char *str, size_t) noexcept {
			return BitInterval{str};
		}
#pragma clang diagnostic pop
	}

}

#endif //BITOPS_INTERVAL_HPP
