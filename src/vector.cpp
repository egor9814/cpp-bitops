//
// Created by egor9814 on 02 Aug 2021.
//

#include <bitops/vector.hpp>
#include <cstring>
#include <stdexcept>

namespace bitops {

	BitVector::BitVector(size_t len) : len(len) {
		if (len == 0) {
			data = nullptr;
			size = 0;
		} else {
			size = (len - 1) / 8 + 1;
			data = new byte_t[size]{0};
		}
	}

	BitVector::BitVector(const char *str)  : data(nullptr), len(0), size(0) {
		setString(str);
	}

	BitVector::BitVector(const BitVector &bv) : len(bv.len), size(bv.size) {
		if (size > 0) {
			data = new byte_t[size];
			for (size_t i = 0; i < size; i++) {
				data[i] = bv.data[i];
			}
		} else {
			data = nullptr;
		}
	}

	BitVector::BitVector(BitVector &&bv) noexcept : data(bv.data), len(bv.len), size(bv.size) {
		bv.data = nullptr;
		bv.len = 0;
		bv.size = 0;
	}

	BitVector &BitVector::operator=(const BitVector &bv) {
		if (this != &bv) {
			delete [] data;
			len = bv.len;
			size = bv.size;
			if (size > 0) {
				data = new byte_t[size];
				for (size_t i = 0; i < size; i++) {
					data[i] = bv.data[i];
				}
			} else {
				data = nullptr;
			}
		}
		return *this;
	}

	BitVector &BitVector::operator=(BitVector &&bv) noexcept {
		if (this != &bv) {
			delete [] data;
			data = bv.data;
			len = bv.len;
			size = bv.size;
			bv.data = nullptr;
			bv.len = 0;
			bv.size = 0;
		}
		return *this;
	}

	BitVector::~BitVector() {
		delete [] data;
	}

	void BitVector::setString(const char *str) noexcept {
		delete [] data;
		data = nullptr;
		if (str) {
			len = static_cast<size_t>(strlen(str));
			size = (len - 1) / 8 + 1;
			if (size > 0) {
				data = new byte_t[size]{0};
				size_t j{size-1};
				byte_t mask{1};
				for (auto i = len; i > 0;) {
					if (!mask) {
						j--;
						mask = 1;
					}
					i--;
					if (str[i] != '0') {
						data[j] |= mask;
					}
					mask <<= 1u;
				}
			}
		} else {
			len = 0;
			size = 0;
		}
	}

	BitVector &BitVector::operator=(const char *str) noexcept {
		setString(str);
		return *this;
	}

	BitVector &BitVector::set0(size_t k) {
		if (data == nullptr)
			throw std::runtime_error{"empty vector"};
		if (k >= len)
			throw std::runtime_error{"out of range"};
		auto i = size - 1 - k / 8;
		k %= 8;
		byte_t mask = byte_t{1} << k;
		data[i] &= ~mask;
		return *this;
	}

	BitVector &BitVector::set1(size_t k) {
		if (data == nullptr)
			throw std::runtime_error{"empty vector"};
		if (k >= len)
			throw std::runtime_error{"out of range"};
		auto i = size - 1 - k / 8;
		k %= 8;
		byte_t mask = byte_t{1} << k;
		data[i] |= mask;
		return *this;
	}

	BitVector &BitVector::invert(size_t k) {
		if (data == nullptr)
			throw std::runtime_error{"empty vector"};
		if (k >= len)
			throw std::runtime_error{"out of range"};
		auto i = size - 1 - k / 8;
		k %= 8;
		byte_t mask = byte_t{1} << k;
		data[i] ^= mask;
		return *this;
	}

	bool BitVector::operator==(const BitVector &other) const noexcept {
		if (this == &other)
			return true;
		if (size != other.size || len != other.len)
			return false;
		for (size_t i = 0; i < size; i++)
			if (data[i] != other.data[i])
				return false;
		return true;
	}

	bool BitVector::operator!=(const BitVector &other) const noexcept {
		if (this == &other)
			return false;
		if (size != other.size || len != other.len)
			return true;
		for (size_t i = 0; i < size; i++)
			if (data[i] != other.data[i])
				return true;
		return false;
	}

	BitVector &BitVector::operator&=(const BitVector &other) noexcept {
		auto minSize = size < other.size ? size : other.size;
		size_t i;
		for (i = 0; i < minSize; i++)
			data[i] &= other.data[i];
		for (; i < size; i++)
			data[i] = 0;
		return *this;
	}

	BitVector BitVector::operator&(const BitVector &other) const noexcept {
		BitVector self{*this};
		self &= other;
		return self;
	}

	BitVector &BitVector::operator|=(const BitVector &other) noexcept {
		auto minSize = size < other.size ? size : other.size;
		for (size_t i = 0; i < minSize; i++)
			data[i] |= other.data[i];
		return *this;
	}

	BitVector BitVector::operator|(const BitVector &other) const noexcept {
		BitVector self{*this};
		self |= other;
		return self;
	}

	BitVector &BitVector::operator^=(const BitVector &other) noexcept {
		auto minSize = size < other.size ? size : other.size;
		for (size_t i = 0; i < minSize; i++)
			data[i] ^= other.data[i];
		return *this;
	}

	BitVector BitVector::operator^(const BitVector &other) const noexcept {
		BitVector self{*this};
		self ^= other;
		return self;
	}

	BitVector BitVector::operator~() const noexcept {
		BitVector res{len};
		for (size_t i = 0; i < size; i++)
			res.data[i] = ~data[i];
		auto k = len % 8;
		if (k) {
			byte_t mask = byte_t{1} << k;
			mask--;
			res.data[0] &= mask;
		}
		return res;
	}

	BitVector &BitVector::operator<<=(size_t k) noexcept {
		if (k == 0)
			return *this;
		if (k >= len) {
			for (size_t i = 0; i < size; i++)
				data[i] = 0;
			return *this;
		}

		auto k1 = k / 8;
		auto k2 = k % 8;
		size_t i;
		for (i = 0; i < size - k1 - 1; i++) {
			data[i] = (data[i+k1] << k2) | (data[i+1+k1] >> (8 - k2));
		}
		data[i] = data[i+k1] << k2;
		for (i++; i < size; i++)
			data[i] = 0;

		k = len % 8;
		if (k) {
			byte_t mask = byte_t{1} << k;
			mask--;
			data[0] &= mask;
		}

		return *this;
	}

	BitVector BitVector::operator<<(size_t k) const noexcept {
		BitVector self{*this};
		self <<= k;
		return self;
	}

	BitVector &BitVector::operator>>=(size_t k) noexcept {
		if (k == 0)
			return *this;
		if (k >= len) {
			for (size_t i = 0; i < size; i++)
				data[i] = 0;
			return *this;
		}

		auto k1 = k / 8;
		auto k2 = k % 8;
		size_t i;
		for (i = size; i > k1 + 1; ) {
			i--;
			data[i] = (data[i-k1] >> k2) | (data[i-1-k1] << (8 - k2));
		}
		i--;
		data[i] = data[i-k1] >> k2;
		for (i--; i < size; i--)
			data[i] = 0;

		return *this;
	}

	BitVector BitVector::operator>>(size_t k) const noexcept {
		BitVector self{*this};
		self >>= k;
		return self;
	}

	BitVector::BitRef BitVector::operator[](size_t k) {
		if (data == nullptr)
			throw std::runtime_error{"empty vector"};
		if (k >= len)
			throw std::runtime_error{"out of range"};
		auto i = size - 1 - k / 8;
		k %= 8;
		return BitRef(&data[i], k);
	}

	BitVector::BitRef BitVector::operator[](size_t k) const {
		if (data == nullptr)
			throw std::runtime_error{"empty vector"};
		if (k >= len)
			throw std::runtime_error{"out of range"};
		auto i = size - 1 - k / 8;
		k %= 8;
		return BitRef(&data[i], k);
	}

	std::string BitVector::toString() const noexcept {
		std::string str(len, '0');
		byte_t mask{1};
		auto k = len % 8;
		if (!k)
			k = 8;
		k--;
		mask <<= k;

		size_t j{0};
		k = 0;
		for (size_t i = 0; i < len; i++) {
			if (!mask) {
				mask = 1;
				mask <<= 7u;
				j++;
			}
			str[k++] = (data[j] & mask) ? '1' : '0';
			mask >>= 1u;
		}

		return str;
	}

	BitVector::operator std::string() const noexcept {
		return toString();
	}

	size_t BitVector::getWeight() const noexcept {
		size_t w = 0;
		for (size_t i = 0; i < size; i++) {
			auto b = data[i];
			while (b) {
				w++;
				b &= (b - 1);
			}
		}
		return w;
	}

	size_t BitVector::getSize() const noexcept {
		return size;
	}

	size_t BitVector::getLength() const noexcept {
		return len;
	}

}
