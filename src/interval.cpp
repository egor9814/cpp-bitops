//
// Created by egor9814 on 02 Aug 2021.
//

#include <bitops/interval.hpp>
#include <cstring>
#include <stdexcept>

namespace bitops {

	BitInterval::BitInterval(size_t len) noexcept : vec(len), dnc(len) {}

	BitInterval::BitInterval(const char *vec, const char *dnc) noexcept {
		if (vec && dnc && strlen(vec) == strlen(dnc)) {
			this->vec.setString(vec);
			this->dnc.setString(dnc);
		}
		// TODO: maybe insert `throw` here?
	}

	BitInterval::BitInterval(const char *str) noexcept {
		setString(str);
	}

	BitInterval::BitInterval(const BitVector &vec, const BitVector &dnc) noexcept {
		setInterval(vec, dnc);
	}

	BitInterval::BitInterval(const BitInterval &bi) = default;

	BitInterval::BitInterval(BitInterval &&bi) noexcept
			: vec(std::move(bi.vec)), dnc(std::move(bi.dnc)) {}

	BitInterval &BitInterval::operator=(const BitInterval &bi) {
		if (this != &bi) {
			vec = bi.vec;
			dnc = bi.dnc;
		}
		return *this;
	}

	BitInterval &BitInterval::operator=(BitInterval &&bi) noexcept {
		if (this != &bi) {
			vec = std::move(bi.vec);
			dnc = std::move(bi.dnc);
		}
		return *this;
	}

	void BitInterval::setInterval(const BitVector &vec, const BitVector &dnc) noexcept {
		if (vec.getLength() == dnc.getLength()) {
			this->vec = vec;
			this->dnc = dnc;
		}
		// TODO: maybe insert `throw` here?
	}

	void BitInterval::setString(const char *interval) noexcept {
		auto l = interval ? static_cast<size_t>(strlen(interval)) : 0;
		vec = BitVector(l);
		dnc = BitVector(l);
		for (size_t i = 0; i < l; i++) {
			auto j = l - i - 1;
			// disabling diagnostic, because if l == 0 (interval == nullptr), then loop not started
#pragma clang diagnostic push
#pragma ide diagnostic ignored "NullDereferences"
			switch (interval[i]) {
				case '-':
					dnc.set1(j);
					break;
				case '0':
					break;
				default:
					vec.set1(j);
					break;
			}
#pragma clang diagnostic pop
		}
	}

	bool BitInterval::operator==(const BitInterval &other) const noexcept {
		return vec == other.vec && dnc == other.dnc;
	}

	bool BitInterval::operator!=(const BitInterval &other) const noexcept {
		return vec != other.vec || dnc != other.dnc;
	}

	std::string BitInterval::toString() const noexcept {
		static char chars[3]{'0', '1', '-'};
		auto l = getLength();
		std::string str(l, '\0');
		for (size_t i = 0; i < l; i++) {
			str[i] = chars[byte_t(get(l - 1 - i))];
		}
		return str;
	}

	BitInterval::operator std::string() const noexcept {
		return toString();
	}

	size_t BitInterval::getLength() const noexcept {
		return vec.getLength();
	}

	size_t BitInterval::getRang() const noexcept {
		return vec.getLength() - dnc.getWeight();
	}

	bool BitInterval::isOrthogonal(const BitInterval &other, bool one) const noexcept {
		auto tmpDnc = dnc | other.dnc;
		auto tmp1 = vec | tmpDnc;
		auto tmp2 = other.vec | tmpDnc;
		auto w = (tmp1 ^ tmp2).getWeight();
		return one ? (w == 1) : (w != 0);
	}

	BitInterval::TriBit BitInterval::get(size_t k) const {
		if (dnc[k])
			return TriBit::X;
		return TriBit(vec[k].get());
	}

	void BitInterval::set(size_t k, BitInterval::TriBit bit) {
		// TODO: remove duplicate code like in TriBitRef::set(TriBit)
		auto dk = dnc[k];
		auto vk = vec[k];
		if (bit == TriBit::X) {
			dk.set();
			vk.reset();
		} else {
			dk.reset();
			vk.set(bool(bit));
		}
	}

	BitInterval::TriBitRef BitInterval::operator[](size_t k) {
		if (k >= getLength())
			throw std::runtime_error{"out of range"};
		return TriBitRef(&vec, &dnc, k);
	}

	BitInterval::TriBitRef BitInterval::operator[](size_t k) const {
		if (k >= getLength())
			throw std::runtime_error{"out of range"};
		// TODO: bad code, replace this with ConstTriBitRef
		auto self = const_cast<BitInterval*>(this);
		return TriBitRef(&self->vec, &self->dnc, k);
	}

	BitInterval &BitInterval::operator|=(const BitInterval &other) noexcept {
		if (this != &other) {
			/*
			 *   000-0-
			 * | 010-0-
			 * = 010-0-
			 *
			 *   00010-
			 * | 010-0-
			 * = 01010-
			 *
			 *
			 *   000100
			 * | 010000
			 * = 010100
			 *
			 *   000001
			 * & 000101
			 * = 000001
			 *
			 * > 01010-
			 *
			 *
			 * 1-1-1- | 1--010 = 1-1010
			 *
			 *   101010
			 * | 100010
			 * = 101010
			 *
			 *   010101
			 * & 011000
			 * = 010000
			 *
			 * > 1-1010
			 * */

			vec |= other.vec;
			dnc &= other.dnc;
		}
		return *this;
	}

	BitInterval BitInterval::operator|(const BitInterval &other) const noexcept {
		auto self{*this};
		self |= other;
		return self;
	}

	BitInterval &BitInterval::operator&=(const BitInterval &other) noexcept {
		if (this != &other) {
			/*
			 * 010-1- & 1-1111 = 0-0-1-
			 *
			 *   010010
			 * & 101111
			 * = 000010
			 *
			 *   000101 |   111010
			 *   010000 | & 101111
			 *          | = 101010 ~= 010101
			 *
			 * > 0-0-1-
			 *
			 * 1-11-- & -11--- = --1---
			 *
			 *   101100
			 * & 011000
			 * = 001000
			 *
			 *   010011 |   101100
			 *   100111 | & 011000
			 *          | = 001000 ~= 110111
			 *
			 * > --1---
			 * */
			vec &= other.vec;
			dnc = ~(~dnc & ~other.dnc);
		}
		return *this;
	}

	BitInterval BitInterval::operator&(const BitInterval &other) const noexcept {
		auto self{*this};
		self &= other;
		return self;
	}

	BitInterval BitInterval::operator~() const noexcept {
		auto self{*this};
		/*
		 * ~ 1-1-1- = 0-0-0-
		 *
		 * ~ 101010
		 * = 010101
		 *
		 * ~ 010101
		 * = 101010
		 *
		 *   010101
		 * & 101010
		 * = 000000
		 *
		 * > 0-0-0-
		 *
		 *
		 * ~ 1--010 = 0--101
		 *
		 * ~ 100010
		 * = 011101
		 *
		 * ~ 011000
		 * = 100111
		 *
		 *   011101
		 * & 100111
		 * = 000101
		 *
		 * > 0--101
		 * */
		self.vec = ~self.vec & ~self.dnc;
		return self;
	}

	BitInterval &BitInterval::operator<<=(size_t k) noexcept {
		vec <<= k;
		dnc <<= k;
		return *this;
	}

	BitInterval BitInterval::operator<<(size_t k) const noexcept {
		auto self{*this};
		self <<= k;
		return self;
	}

	BitInterval &BitInterval::operator>>=(size_t k) noexcept {
		vec >>= k;
		dnc >>= k;
		return *this;
	}

	BitInterval BitInterval::operator>>(size_t k) const noexcept {
		auto self{*this};
		self >>= k;
		return self;
	}

	const BitVector &BitInterval::getVector() const noexcept {
		return vec;
	}

	const BitVector &BitInterval::getDnc() const noexcept {
		return dnc;
	}

}
