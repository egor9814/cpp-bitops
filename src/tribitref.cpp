//
// Created by egor9814 on 03 Aug 2021.
//

#include <bitops/interval.hpp>

namespace bitops {

	BitInterval::TriBitRef::TriBitRef(BitVector *vec, BitVector *dnc, size_t index)
			: vec(vec), dnc(dnc), index(index) {}

	BitInterval::TriBitRef &BitInterval::TriBitRef::operator=(const BitInterval::TriBitRef &ref) {
		if (this != &ref) {
			set(ref.get());
		}
		return *this;
	}

	BitInterval::TriBitRef::TriBitRef(BitInterval::TriBitRef &&ref) noexcept
			: vec(ref.vec), dnc(ref.dnc), index(ref.index) {
		ref.vec = nullptr;
		ref.dnc = nullptr;
		ref.index = 0;
	}

	BitInterval::TriBitRef &BitInterval::TriBitRef::operator=(BitInterval::TriBitRef &&ref) noexcept {
		if (this != &ref) {
			vec = nullptr;
			std::swap(vec, ref.vec);

			dnc = nullptr;
			std::swap(dnc, ref.dnc);

			index = ref.index;
			ref.index = 0;
		}
		return *this;
	}

	BitInterval::TriBitRef &BitInterval::TriBitRef::set(BitInterval::TriBit bit) {
		auto di = (*dnc)[index];
		auto vi = (*vec)[index];
		if (bit == TriBit::X) {
			di.set();
			vi.reset();
		} else {
			di.reset();
			vi.set(bool(bit));
		}
		return *this;
	}

	BitInterval::TriBitRef &BitInterval::TriBitRef::reset() {
		set(TriBit::Zero);
		return *this;
	}

	BitInterval::TriBitRef &BitInterval::TriBitRef::x() {
		set(TriBit::X);
		return *this;
	}

	BitInterval::TriBit BitInterval::TriBitRef::get() const {
		if ((*dnc)[index].get()) {
			return TriBit::X;
		}
		return TriBit((*vec)[index].get());
	}

	BitInterval::TriBitRef &BitInterval::TriBitRef::operator=(BitInterval::TriBit bit) {
		set(bit);
		return *this;
	}

	BitInterval::TriBitRef::operator BitInterval::TriBit() const {
		return get();
	}
}
