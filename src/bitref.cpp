//
// Created by egor9814 on 02 Aug 2021.
//

#include <bitops/vector.hpp>

namespace bitops {

	BitVector::BitRef::BitRef(byte_t *byte, byte_t shift) : byte(byte), shift(shift) {}

	BitVector::BitRef &BitVector::BitRef::operator=(const BitVector::BitRef &ref) {
		if (this != &ref) {
			set(ref.get());
		}
		return *this;
	}

	BitVector::BitRef::BitRef(BitVector::BitRef &&ref) noexcept : byte(ref.byte), shift(ref.shift) {
		ref.byte = nullptr;
		ref.shift = 0;
	}

	BitVector::BitRef &BitVector::BitRef::operator=(BitVector::BitRef &&ref) noexcept {
		if (this != &ref) {
			byte = nullptr;
			shift = 0;
			std::swap(byte, ref.byte);
			std::swap(shift, ref.shift);
		}
		return *this;
	}

	BitVector::BitRef &BitVector::BitRef::set(bool bit) noexcept {
		byte_t mask = byte_t{1} << shift;
		if (bit) {
			*byte |= mask;
		} else {
			*byte &= ~mask;
		}
		return *this;
	}

	BitVector::BitRef &BitVector::BitRef::reset() noexcept {
		return set(false);
	}

	bool BitVector::BitRef::get() const noexcept {
		byte_t mask = byte_t{1} << shift;
		return (*byte & mask) != 0;
	}

	BitVector::BitRef &BitVector::BitRef::operator=(bool bit) noexcept {
		set(bit);
		return *this;
	}

	BitVector::BitRef::operator bool() const noexcept {
		return get();
	}

}
